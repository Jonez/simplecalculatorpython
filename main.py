import numpy as np
import re


def add(expected, calc):
    custom_delimiter = ''
    multi_delimiter = ''
    if calc == '':
        calc = 'There is no value'
    else:
        if calc.startswith('//'):
            calc = calc.replace('//', '')
            if calc[0] != '[':
                custom_delimiter = calc[0]
            else:
                multi_delimiter = re.findall(r'\[([^]]*)\]', calc)
                for i in range(len(multi_delimiter)):
                    multi_delimiter[i] += '|'
                multi_delimiter = ''.join(multi_delimiter)
                calc = calc.replace('[', '')
                calc = calc.replace(']', '')

        regex = multi_delimiter + '[' + custom_delimiter + ',\n]'
        arr = np.array(re.split(regex, calc))
        arr = np.array([i for i in arr if i]).astype(int)
        arr = arr[arr <= 1000]

        if True in arr[arr < 0].astype(bool):
            calc = 'Negatives not allowed'
        else:
            calc_sum = 0
            for i in range(len(arr)):
                calc_sum += int(arr[i])
                calc = str(calc_sum)

    return expected + calc


if __name__ == '__main__':
    print(add('Expected result, There is no value: ', ''))  # no value
    print(add('Expected result, 4: ', '4'))  # single number
    print(add('Expected result, 3: ', '1,2'))  # Two number addition
    print(add('Expected result, 45: ', '1,2,3,4,5,6,7,8,9'))  # Multiple numbers
    print(add('Expected result, 6: ', '1\n2,3'))  # New line
    print(add('Expected result, 3: ', '//;\n1;2'))  # Custom Separator
    print(add('Expected result, Negatives not allowed: ', '1,-2,-3'))  # Disallow Negatives
    print(add('Expected result, 2: ', '1001,2'))  # Ignore Bigger Than Thousand
    print(add('Expected result, 6: ', '//[foo][bar]\n1foo2bar3'))  # Multiple Custom Separators
    print(add('Expected result, 21: ', '//[x][y][Matthew][Kenneth][Jones]\n1x2y3Matthew2Kenneth4Jones9'))  # My Own Test
